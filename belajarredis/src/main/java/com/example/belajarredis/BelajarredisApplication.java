package com.example.belajarredis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BelajarredisApplication {

	public static void main(String[] args) {
		SpringApplication.run(BelajarredisApplication.class, args);
	}

}
