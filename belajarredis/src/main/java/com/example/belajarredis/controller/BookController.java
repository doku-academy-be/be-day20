package com.example.belajarredis.controller;

import com.example.belajarredis.model.BaseResponse;
import com.example.belajarredis.model.Book;
import com.example.belajarredis.model.BookRequest;
import com.example.belajarredis.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("/books")
public class BookController {
    @Autowired
    private BookRepository bookRepository;

    @GetMapping("")
    public BaseResponse<List<Book>> getAll(){
        List<Book> books = new ArrayList<>();
        bookRepository.findAll().forEach(books::add);

        BaseResponse<List<Book>> response = new BaseResponse<>(
                "success",
                "all books",
                books
        );
        return response;
    }

    @PostMapping("")
    public BaseResponse<Book> create(@RequestBody BookRequest request){
        UUID uuid = UUID.randomUUID();
        Book book = new Book(
                uuid.toString(),
                request.getIsbn(),
                request.getJudul(),
                request.getPenulis(),
                request.getDeskripsi(),
                request.getKategori()
        );
        Book createdBook = bookRepository.save(book);
        BaseResponse<Book> response = new BaseResponse<>(
                "success",
                "all books",
                createdBook
        );
        return response;
    }
    @GetMapping("/{id}")
    public BaseResponse<Book> getID(@PathVariable(name="id")String id){
        Book getBookID = bookRepository.findById(id).orElse(null);
        return new BaseResponse<>(
                "success",
                "books",
                getBookID
        );
    }
    @PutMapping("/{id}")
    public BaseResponse<Book> update(@PathVariable(name="id") String id, @RequestBody Book request){
        Optional<Book> bookOptional = bookRepository.findById(id);
        if (bookOptional.isPresent()){
            Book book = bookOptional.get();
            book.setIsbn(request.getIsbn());
            book.setJudul(request.getJudul());
            book.setPenulis(request.getPenulis());
            book.setDeskripsi(request.getDeskripsi());
            book.setKategori(request.getKategori());
            Book updateBook = bookRepository.save(book);
            return new BaseResponse<>(
                    "success",
                    "books created",
                    updateBook
            );
        }
        return new BaseResponse<>(
                "failed",
                "books created",
                request
        );
    }

    @DeleteMapping("/{id}")
    public HttpStatus delete(@PathVariable(name = "id") String id){
        bookRepository.deleteById(id);
        return HttpStatus.OK;
    }
}
