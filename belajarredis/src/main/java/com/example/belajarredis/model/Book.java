package com.example.belajarredis.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.redis.core.RedisHash;

@Getter
@Setter
@RedisHash("book")
public class Book {
    private String id;
    private String isbn;
    private String judul;
    private String penulis;
    private String deskripsi;
    private String kategori;

    public Book(String id, String isbn, String judul, String penulis, String deskripsi, String kategori) {
        this.id = id;
        this.isbn = isbn;
        this.judul = judul;
        this.penulis = penulis;
        this.deskripsi = deskripsi;
        this.kategori = kategori;
    }
}
