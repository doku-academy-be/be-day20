package com.example.belajarredis.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BookRequest {
    private String isbn;
    private String judul;
    private String penulis;
    private String deskripsi;
    private String kategori;
}
